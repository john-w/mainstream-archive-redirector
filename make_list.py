#!/usr/bin/env python

import json
import re
from datetime import datetime

RULES = {
    "createdBy": __file__,
    "createdAt": datetime.now().isoformat(),
    "redirects": []
}
ENTRY_TEMPLATE = {
    "description": "%s",
    "exampleUrl": "%s",
    "exampleResult": "https://archive.today/newest/%s",
    "error": None,
    "includePattern": "^(https?://[a-zA-Z0-9.]*%s.*)",
    "excludePattern": "",
    "patternDesc": "",
    "redirectUrl": "https://archive.today/newest/$1",
    "patternType": "R",
    "processMatches": "noProcessing",
    "disabled": False,
    "grouped": False,
    "appliesTo": [
        "main_frame",
        "sub_frame",
        "imageset",
        "object",
        "xmlhttprequest",
        "history",
        "other"
    ]
}


def make_entry(url: str, template: dict = ENTRY_TEMPLATE) -> dict:
    rgx = (
        r"^https?://"
        r"(?:www.)?"
        r"("
            r"[a-zA-Z0-9]+"
            r"(?:.[a-zA-Z0-9])+/"
        r")"
        r".*$"
    )

    domain = url
    matcher = re.match(rgx, url)
    if matcher:
        domain = matcher[1]

    description = "Archiver: MSM %s" % domain.split('.')[0].title()

    return json.loads(
        json.dumps(template) % (description, url, url, domain)
    )

if __name__ == '__main__':
    with open('redirector.domain.list') as fp:
        for url in fp.readlines():
            stripped = url.strip()

            print(":: Found %s" % stripped)
            rule = make_entry(stripped)
            RULES['redirects'].append(rule)

    print(":: Dumping redirector rules file")
    with open('rules.json', 'w') as fp:
        json.dump(RULES, fp)
    
